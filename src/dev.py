
import sys
import numpy as np

import ycb
import viz

import pdb

def fetch_tsrs(uuid, tf):
    ycb_object = ycb.YCBObject.from_uuid(uuid)

    tsrs = []

    for primitive in ycb_object.primitives:
        for tsr in primitive.tsrs:
            tsr.T_o_w = np.dot(tf, tsr.T_o_w)
            tsrs.append(tsr)

    return tsrs

def main():

    xuuid = 0

    if len(sys.argv) == 2:
        uuid = int(sys.argv[1])
    else:
        uuid = xuuid

    tf   = np.eye(4)

    orange = ycb.YCBObject.from_uuid(uuid)

    size = 0.5
    fig, ax = viz.get_plot([-size, size], [-size, size], [-size, size])

    for primitive in orange.primitives:
        primitive.render(ax)

        for tsr in primitive.tsrs:
            nof_samples = 100 / len(primitive.tsrs)
            tsr.render(ax, primitive.scale, nof_samples)

    viz.render()

if __name__ == '__main__':
    main()
