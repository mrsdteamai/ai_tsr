
import numpy as np
import math

def make_transform(position, angle):
    tf = np.eye(4)

    if isinstance(angle, list) and \
       (isinstance(angle[0], list) or isinstance(angle[0], tuple)):
        for angl in angle:
            tf = np.dot(make_transform(None, angl), tf)

        tf[:3,3:] = np.array([position]).T
    else:
        axis, angl = angle

        xtf = [[ np.cos(angl),-np.sin(angl)],
               [ np.sin(angl), np.cos(angl)]]
        xtf = np.array(xtf)

        if axis.lower() == 'yaw':
            tf[:2,:2] = xtf
        elif axis.lower() == 'pitch':
            tf[-2:-5:-2,-2:-5:-2] = xtf
        elif axis.lower() == 'roll':
            tf[1:3,1:3] = xtf
        else:
            tf = np.eye(4)

    return tf

def decode_transform(R):
    sy = math.sqrt(R[0, 0] * R[0, 0] + R[1, 0] * R[1, 0])

    singular = sy < 1e-6

    if not singular:
        x = math.atan2(R[2, 1], R[2, 2])
        y = math.atan2(-R[2, 0], sy)
        z = math.atan2(R[1, 0], R[0, 0])
    else:
        x = math.atan2(-R[1, 2], R[1, 1])
        y = math.atan2(-R[2, 0], sy)
        z = 0

    return np.array([x, y, z])

