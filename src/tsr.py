
import numpy as np

import tform

import pdb

import viz

class TSR(object):
    def __init__(self, T_o_w, T_w_e, B_w):
        self.T_o_w = T_o_w
        self.T_w_e = T_w_e
        self.B_w   = B_w

    def render(self, ax, scale=1.0, nof_samples=10):
        viz.render_frame(ax, self.T_o_w, scale)
        # viz.render_frame(ax, np.dot(self.T_o_w, self.T_w_e), scale)

        for i in range(nof_samples):
            x, y, z, roll, pitch, yaw = \
                  np.random.uniform(size=(6,))*np.ptp(self.B_w, axis=1) \
                + self.B_w[:,0]

            position = [float(x), float(y), float(z)]
            angle    = [['roll', float(roll)], 
                        ['pitch', float(pitch)],
                        ['yaw', float(yaw)]]
            T_w_s = tform.make_transform(position, angle)

            tf = np.dot(np.dot(self.T_o_w, T_w_s), self.T_w_e)
            viz.render_frame(ax, tf, scale)
