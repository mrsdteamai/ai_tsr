#!/usr/bin/python
import sys
from ai_tsr.srv import *
from ai_tsr.msg import *
import numpy as np

import ycb
import viz
import rospy

def tsr_client(tf,uuid):
    rospy.wait_for_service('tsrs')
    try:
        tsrs = rospy.ServiceProxy('tsrs', tsr_srv)
        resp1 = tsrs(tf,uuid)

        return resp1.tsrs
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e



if __name__ == "__main__":
    tf = tf_t()
    tf.x = 1
    tf.y = 1
    tf.z = 1
    tf.yaw = 1
    tf.pitch = 1
    tf.roll = 1

    uuid = 1

    print "Requesting %s"%(uuid)
    tsrs = tsr_client(tf,uuid)
    print tsrs



