#!/usr/bin/python
from ai_tsr.srv import *
from ai_tsr.msg import *
import numpy as np
import math
import ycb
import viz
import rospy
import tform
import pdb

def fetch_tsrs(tf,uuid):
    ycb_object = ycb.YCBObject.from_uuid(uuid)

    tsrs_msg = []


    for primitive in ycb_object.primitives:
        for t in primitive.tsrs:
            tsr_msg = tsr()
            t.T_o_w = np.dot(tf, t.T_o_w)
            # print t.T_o_w
            roll,pitch,yaw = tform.decode_transform(t.T_o_w[0:3,0:3])

            tsr_msg.T_o_w.x, tsr_msg.T_o_w.y, tsr_msg.T_o_w.z = t.T_o_w[0:3,3]
            tsr_msg.T_o_w.roll, tsr_msg.T_o_w.pitch, tsr_msg.T_o_w.yaw = roll,pitch,yaw

            tsr_msg.T_w_e.x, tsr_msg.T_w_e.y,tsr_msg.T_w_e.z = t.T_w_e[0:3,3]

            roll2,pitch2,yaw2= tform.decode_transform(t.T_w_e[0:3,0:3])

            # tsr_msg.T_w_e.roll = roll2
            tsr_msg.T_w_e.roll, tsr_msg.T_w_e.pitch, tsr_msg.T_w_e.yaw = roll2,pitch2,yaw2

            # b = bound()
            # print t.B_w
            tsr_msg.B_w.x_b.lower, tsr_msg.B_w.x_b.upper = t.B_w[0,:]
            tsr_msg.B_w.y_b.lower, tsr_msg.B_w.y_b.upper = t.B_w[1, :]
            tsr_msg.B_w.z_b.lower, tsr_msg.B_w.z_b.upper = t.B_w[2, :]
            tsr_msg.B_w.roll_b.lower, tsr_msg.B_w.roll_b.upper = t.B_w[3,:]
            tsr_msg.B_w.pitch_b.lower, tsr_msg.B_w.pitch_b.upper = t.B_w[4, :]
            tsr_msg.B_w.yaw_b.lower, tsr_msg.B_w.yaw_b.upper = t.B_w[5, :]

            tsrs_msg.append(tsr_msg)

    print "all of tsrs"
    # print tsrs_msg

    return tsrs_msg

def handle_tsr(req):
    print "tsr"

    tf = [req.tf.x, req.tf.y, req.tf.z, req.tf.roll, req.tf.pitch, req.tf.yaw]

    tf = tform.make_transform(
        tf[:3],
        zip(['roll', 'pitch', 'yaw'], tf[3:]))
    # print type(fetch_tsrs(tf,req.uuid))
    # print type(tsr_srvResponse)
    print fetch_tsrs(tf,req.uuid)
    return tsr_srvResponse(fetch_tsrs(tf,req.uuid))

def tsr_server():
    rospy.init_node('tsr_server')
    s = rospy.Service('tsrs', tsr_srv, handle_tsr)
    print "Ready to get tsrs."
    rospy.spin()

if __name__ == "__main__":
    tsr_server()