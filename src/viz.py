
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np

import pdb

def get_plot(xlims, ylims, zlims):
    fig = plt.figure()
    ax  = fig.add_subplot(111, projection='3d')

    ax.set_xlim(*xlims)
    ax.set_ylim(*ylims)
    ax.set_zlim(*zlims)

    ax.axis('off')

    return fig, ax

def render_sphere(ax, tf, radius, color):
    u = np.linspace(0, 2 * np.pi, 60)
    v = np.linspace(0, np.pi, 60)

    x = tf[0,3] + radius * np.outer(np.cos(u), np.sin(v))
    y = tf[1,3] + radius * np.outer(np.sin(u), np.sin(v))
    z = tf[2,3] + radius * np.outer(np.ones(np.size(u)), np.cos(v))

    ax.plot_surface(x, y, z, color=color)

def render_cuboid(ax, tf, hwd, color):
    points = np.array([[-1, -1, -1],
                      [1, -1, -1 ],
                      [1, 1, -1],
                      [-1, 1, -1],
                      [-1, -1, 1],
                      [1, -1, 1 ],
                      [1, 1, 1],
                      [-1, 1, 1]])

    h, w, d = hwd 

    r = [-1,1]
    X, Y = np.meshgrid(r, r)
    ax.plot_surface( X * d/2., Y * w/2., 1 * h/2., color=color)
    ax.plot_surface( X * d/2., Y * w/2.,-1 * h/2., color=color)
    ax.plot_surface( X * d/2.,-1 * w/2., Y * h/2., color=color)
    ax.plot_surface( X * d/2., 1 * w/2., Y * h/2., color=color)
    ax.plot_surface( 1 * d/2., X * w/2., Y * h/2., color=color)
    ax.plot_surface(-1 * d/2., X * w/2., Y * h/2., color=color)

def render_cylinder(ax, tf, radius, height, color):
    N = 100             
    cstride_side = 1000 
    rstride_side = 1    
    cstride_top = 10    
    rstride_top = 10

    phi = np.linspace(0, 2 * np.pi, N) 
    _r = np.ones(N) 
    _h = np.linspace(-height/2., height/2., N) 

    _x = radius * np.outer(np.cos(phi), _r)
    _y = radius * np.outer(np.sin(phi), _r) 
    _z = np.outer(np.ones(np.size(_r)), _h)
    ax.plot_surface(_x, _y, _z, rstride=rstride_side, cstride=cstride_side, linewidth=0, alpha=1, color=color) 

    _x *= 0.99
    _y *= 0.99
    ax.plot_surface(_x, _y, _z, rstride=rstride_side+1, cstride=cstride_side+1, linewidth=0, alpha=1, color=color) 

    _x = radius * np.outer(np.cos(phi), _h) / (height/2.)
    _y = radius * np.outer(np.sin(phi), _h) / (height/2.) 
    _z = np.zeros([N,N]) + (height/2.)
    ax.plot_surface(_x, _y, _z,  rstride=rstride_top, cstride=cstride_top, linewidth=0, alpha=1, color=color) 
    
    _x = radius * np.outer(np.cos(phi), _h) / (height/2.)
    _y = radius * np.outer(np.sin(phi), _h) / (height/2.)
    _z = np.zeros([N,N]) - (height/2.)
    ax.plot_surface(_x, _y, _z,  rstride=rstride_top, cstride=cstride_top, linewidth=0, alpha=1, color=color) 

def render_frame(ax, tf, scale=1.0):
    ax.quiver(tf[0,3], tf[1,3], tf[2,3], 
              scale*tf[0,0], scale*tf[1,0], scale*tf[2,0],
              color='b')
    ax.quiver(tf[0,3], tf[1,3], tf[2,3], 
              scale*tf[0,1], scale*tf[1,1], scale*tf[2,1],
              color='g')
    ax.quiver(tf[0,3], tf[1,3], tf[2,3], 
              scale*tf[0,2], scale*tf[1,2], scale*tf[2,2],
              color='r')

def render():
    plt.show()