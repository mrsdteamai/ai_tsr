
import numpy as np
import os
import yaml

import tform
import tsr
import viz

import pdb

DIST = 0.15

OBJECT_STORE = 'config/blueprints.yaml'

def only_if_valid(f):
    def wrapper(*args, **kwargs):
        #pdb.set_trace()
        if args[0].valid is True:
            f(*args, **kwargs)
        else:
            msg = '\n\n'+args[0].name+' is an invalid object\n\nRules:\n'+args[0].rules
            raise Exception(msg)

    return wrapper

class YCBPrimitive(object):
    def __init__(self, tf, params, constr, name='default', *args, **kwargs):
        self.name   = name
        self.tf     = tform.make_transform(
                        tf[:3],
                        zip(['roll', 'pitch', 'yaw'], tf[3:]))
        self.params = params
        self.constr = []
        self.tsrs   = []

        self.parse_constr(constr)
        self.make_tsrs()

        self.valid  = True
        self.rules  = ''

        self.check_params()
        self.check_constr()

    def check_params(self):
        pass

    def check_constr(self):
        if self.constr == []:
            self.valid = False

    def is_valid(self):
        return self.valid

    def parse_constr(self, constr):
        for const in constr:
            const_T = tform.make_transform(
                        [eval(v.replace('dist', str(DIST))) if isinstance(v, str) else v for v in const['T'][:3]],
                        zip(['roll', 'pitch', 'yaw'], const['T'][3:]))
            const_B = np.array([[eval(v.replace('dist', str(DIST))) if isinstance(v, str) else v for v in V] for V in const['B']])
            self.constr.append({'T': const_T.copy(),
                                'B': const_B.copy()})

    def make_tsrs(self):
        for const in self.constr:
            xtsr = tsr.TSR(*(self.tf, const['T'], const['B']))
            self.tsrs.append(xtsr)

    def render(self, ax):
        pass

    @only_if_valid
    def test(self):
        print self.name, 'is a valid object'


class YCBSphere(YCBPrimitive):
    def __init__(self, tf, params, constr, name='default', *args, **kwargs):
        super(YCBSphere, self).__init__(tf, params, constr, name)

        self.scale = self.params['radius']*1.2

        self.rules = '1) radius <= '+str(DIST)

    def check_params(self):
        if self.params['radius'] > DIST:
            self.valid = False

    def render(self, ax):
        viz.render_sphere(ax, self.tf, self.params['radius'], (0.5, 0.5, 0.5))


class YCBCuboid(YCBPrimitive):
    def __init__(self, tf, params, constr, name='default', *args, **kwargs):
        super(YCBCuboid, self).__init__(tf, params, constr, name)

        self.scale = min([self.params['height'], self.params['width'], self.params['depth']])*0.9

        self.rules = ''

    def check_params(self):
        pass

    def render(self, ax):
        hwd = (self.params['height'], self.params['width'], self.params['depth'])
        viz.render_cuboid(ax, self.tf, hwd, (0.5, 0.5, 0.5))


class YCBCylinder(YCBPrimitive):
    def __init__(self, tf, params, constr, name='default', *args, **kwargs):
        super(YCBCylinder, self).__init__(tf, params, constr, name)

        self.scale = min([self.params['radius'], self.params['height']])*1.2

        self.rules = '1) radius <= '+str(DIST)

    def check_params(self):
        if self.params['radius'] > DIST:
            self.valid = False

    def render(self, ax):
        viz.render_cylinder(ax, self.tf, self.params['radius'], self.params['height'], (0.5, 0.5, 0.5))


class YCBCustom(YCBPrimitive):
    def __init__(self, tf, params, constr, name='default', *args, **kwargs):
        super(YCBCustom, self).__init__(tf, params, constr, name)

        self.scale = 1.

        self.rules = ''

    def check_params(self):
        pass

    def render(self):
        pass


class YCBObject(object):
    def __init__(self, primitives):
        self.primitives = primitives

    def get_primitives(self):
        return self.primitives

    @classmethod
    def from_uuid(tf, uuid):
        base = os.sep.join(__file__.split(os.sep)[:-2])
        object_path = os.path.realpath(os.path.join(base, OBJECT_STORE))
        with open(object_path, 'r') as stream:
            blueprints_list = yaml.load(stream)

        blueprints = blueprints_list[uuid]

        primitives = [OBJECT_CLS[blueprint['cls']](**blueprint['args'])
                        for blueprint in blueprints]

        for primitive in primitives:
            #pdb.set_trace()
            primitive.test()

        cls = YCBObject(primitives)

        return cls


OBJECT_CLS   = {'sphere':   YCBSphere,
                'cuboid':   YCBCuboid,
                'cylinder': YCBCylinder,
                'custom':   YCBCustom}


